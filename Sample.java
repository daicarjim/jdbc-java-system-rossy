    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.sql.Statement;

// Clase principal    
    public class Sample
    {
      // Metodo principal
      public static void main(String[] args) 
      {
        Connection connection = null;
        // Cargar el driver de la base de datos
        try
        {
          // crear una conexion a la base de datos
          connection = DriverManager.getConnection("url-base-de-datos", "usuario", "clave");
          Statement statement = connection.createStatement();
          // Variables para la respectiva data segun lo que requiera la CRUD
          String nombre 
          String apellido
          String password 
          
          statement.setQueryTimeout(30);  // tiempo de espera de la base de datos
          
          statement.executeUpdate("DROP TABLE IF EXISTS USER");
          statement.executeUpdate("CREATE TABLE USER (ID INTEGER PRIMARY KEY AUTOINCREMENT, FIRSTNAME TEXT, LASTNAME TEXT, PASSWORD TEXT)");
          statement.executeUpdate("INSERT INTO USER (FIRSTNAME, LASTNAME, PASSWORD)values(nombre, apellido, password)");
          ResultSet rs = statement.executeQuery("SELECT * FROM USER");
          while(rs.next())
          {
            // mostrar los datos de la base de datos
            System.out.println("name = " + rs.getString("FIRSTNAME"));
            System.out.println("id = " + rs.getInt("ID"));
          }
        }
        catch(SQLException e)
        {
          // si este error se muestra es porque no alcanza memoria
          // esto significa que no se encontro la base de datos
          System.err.println(e.getMessage());
        }
        finally
        {
          try
          {
            if(connection != null)
              connection.close();
          }
          catch(SQLException e)
          {
            // conexion fallida
            System.err.println(e);
          }
        }
      }
    }
